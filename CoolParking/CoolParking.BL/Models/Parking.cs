﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        private static Parking instance;
        /// <summary>
        /// Acquires the mutual-exclusion lock for a given object, executes a statement block, and then releases the lock
        /// </summary>
        private static readonly object locker = new object();
        /// <summary>
        /// Stores all the vehicles at the Parking.
        /// </summary>
        public List<Vehicle> vehicles = new List<Vehicle>();
        /// <summary>
        /// Stores the amount of money earned since the start of the Parking.
        /// </summary>
        public decimal balance = 0;
        Parking()
        {

        }

        public static Parking Instance
        {
            get
            {
                lock (locker)
                {
                    return instance ?? (instance = new Parking());
                }
            }
        }

        public void ResetParking()
        {
            instance = null;
        }
    }
}
