﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using System;
using System.Timers;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        /// <summary>
        /// Stores the value for Timer interval.
        /// </summary>
        public double Interval { get; set; }
        /// <summary>
        /// Generates an event after the set interval.
        /// </summary>
        private Timer timer = new Timer();
        /// <summary>
        /// Timer Elapsed event handler.
        /// </summary>
        public event ElapsedEventHandler Elapsed;

        public TimerService()
        {

        }

        public TimerService(int _interval)
        { Interval = _interval; }

        /// <summary>
        /// Starts raising the Timer Elapsed event.
        /// </summary>
        public void Start()
        {
            timer.Interval = Interval;
            timer.Elapsed += Elapsed;
            timer.Start();
        }
        /// <summary>
        /// Stops raising the Timer Elapsed event.
        /// </summary>
        public void Stop()
        {
            timer.Stop();
        }
        /// <summary>
        /// Releases the resources allocated for timer.
        /// </summary>
        public void Dispose()
        {
            timer.Dispose();
        }

    }
}

