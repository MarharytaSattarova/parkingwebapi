using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http.Extensions;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace CoolParking.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VehiclesController : Controller
    {
        private static ParkingService parkingService;

    public VehiclesController(ParkingService parkServ)
    {
        parkingService = parkServ;
    }

        [HttpGet]
        public ActionResult<ReadOnlyCollection<Vehicle>> Get()
        {
            return parkingService.GetVehicles();
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> Get(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}\-\d{4}\-[A-Z]{2}$"))
                return BadRequest();

            if (Parking.Instance.vehicles.Find(v => v.Id == id) == null)
                return NotFound();

            return Parking.Instance.vehicles.Find(v => v.Id == id);
        }

        [HttpPost]
        public ActionResult<Vehicle> PostData([FromBody] Vehicle newVehicle)
        {
            try
            {
                parkingService.AddVehicle(newVehicle);
                return Created($"{Request.GetDisplayUrl()}/{newVehicle.Id}", newVehicle);
            }
            catch (Exception)
            {
                return BadRequest();
            }
            
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            if (!Regex.IsMatch(id, @"^[A-Z]{2}\-\d{4}\-[A-Z]{2}$"))
                return BadRequest();
            try
            {
                parkingService.RemoveVehicle(id);
            }
            catch (ArgumentException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}